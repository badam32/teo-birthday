import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { GiftService } from '../gifts/gift.service';
import { challenges } from './challenges.model';

@Injectable({
  providedIn: 'root'
})
export class ChallengeGuardService implements CanActivate {

  constructor(private router: Router, private giftService: GiftService) { }
  canActivate(route: ActivatedRouteSnapshot): Promise<boolean> | boolean {
    return true;
    if (!route.data.day) return this.redirect();
    const day: Date = route.data.day;
    const today = new Date();
    const todayChallenge = challenges.find(c => today.getDate() === c.date.getDate() && today.getMonth() === c.date.getMonth());
    return new Promise<boolean>(resolve => {
      if (!this.giftService.isLoaded.value) {
        this.giftService.isLoaded.subscribe(isLoaded => {
          if (!isLoaded) return;
          if (this.giftService.availableGifts.value.findIndex(g => g.id === todayChallenge.gifts[0].id) !== -1)
            return resolve(this.completed());
          if (today.getDate() === day.getDate() && today.getMonth() === day.getMonth()) return resolve(true);
            return resolve(this.redirect());
        });
      }
      else if (this.giftService.availableGifts.value.findIndex(g => g.id === todayChallenge.gifts[0].id) !== -1)
        return resolve(this.completed());

      if (today.getDate() === day.getDate() && today.getMonth() === day.getMonth()) return resolve(true);
         return resolve(this.redirect());
    });
  }

  private redirect(): boolean {
    this.router.navigateByUrl('/challenge/' + (new Date().getMonth() + 1) + '' + new Date().getDate());
    return false;
  }

  private completed(): boolean {
    this.router.navigateByUrl('/completed');
    return false;
  }
}
