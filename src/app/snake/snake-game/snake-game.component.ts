import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import { Game } from '../snake.model';

@Component({
  selector: 'app-snake-game',
  templateUrl: './snake-game.component.html',
  styleUrls: ['./snake-game.component.css']
})
export class SnakeGameComponent implements OnInit {
  readonly size = 20;
  game: Game;
  @Input() width: number = 30;
  @Input() speed: number = 200;
  @Output() grown = new EventEmitter<number>();
  @Output() lost = new EventEmitter<void>();
  snakeSize = 1;

  constructor() {
    window.addEventListener('keydown', function(e) {
      // space and arrow keys
        // tslint:disable-next-line: deprecation
        if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
            e.preventDefault();
        }
    }, false);
  }

  ngOnInit() {
    this.game = new Game(this.width, this.speed, () => {
      this.snakeSize++;
      this.grown.next(this.snakeSize);
    }, () => {
      this.lost.emit();
    });
  }

  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowLeft' : this.game.setNextTranslation('LEFT'); break;
      case 'ArrowRight' : this.game.setNextTranslation('RIGHT'); break;
      case 'ArrowUp' : this.game.setNextTranslation('UP'); break;
      case 'ArrowDown' : this.game.setNextTranslation('DOWN'); break;
    }
  }

}
