import { Component, OnInit } from '@angular/core';
import { elephant, twentyOne } from 'src/app/nonogram/levels';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1110',
  templateUrl: './c1110.component.html',
  styleUrls: ['./c1110.component.css']
})
export class C1110Component implements OnInit {

  level: string = twentyOne;
  won: boolean = false;
  lost: boolean = false;

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result(res: boolean) {
    if (res) {
      this.won = true;
      this.giftService.receiveGift();
    }
    else this.lost = true;
  }

}
