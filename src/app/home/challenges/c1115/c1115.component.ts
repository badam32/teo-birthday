import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1115',
  templateUrl: './c1115.component.html',
  styleUrls: ['./c1115.component.css']
})
export class C1115Component implements OnInit {

  won = false;
  lost = false;
  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result (res: boolean) {
    if (res) {
      this.won = true;
      this.giftService.receiveGift();
    }
    else this.lost = true;
  }

}
