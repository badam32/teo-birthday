import { Component, OnInit } from '@angular/core';
import { elephant, twentyOne } from 'src/app/nonogram/levels';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1111',
  templateUrl: './c1111.component.html',
  styleUrls: ['./c1111.component.css']
})
export class C1111Component implements OnInit {

  level: string = twentyOne;
  won: boolean = false;
  lost: boolean = false;

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  win() {
      this.won = true;
      this.giftService.receiveGift();
  }

  nonogramresult(event: boolean) {
    if (!event)
      this.lost = true;
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }

}
