import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';
import { bordeaux, lama } from 'src/app/nonogram/levels';

@Component({
  selector: 'app-c1121',
  templateUrl: './c1121.component.html',
  styleUrls: ['./c1121.component.css']
})
export class C1121Component implements OnInit {
  level: string = lama;
  won: boolean = false;
  lost: boolean = false;

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  win() {
      this.won = true;
      this.giftService.receiveGift();
  }

  nonogramresult(event: boolean) {
    if (!event)
      this.lost = true;
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }

}
