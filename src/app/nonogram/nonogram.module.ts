import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NonogramGameComponent } from './nonogram-game/nonogram-game.component';

@NgModule({
  declarations: [NonogramGameComponent],
  imports: [
    CommonModule
  ],
  exports: [
    NonogramGameComponent
  ]
})
export class NonogramModule { }
