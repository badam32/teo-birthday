import { Component, OnInit } from '@angular/core';
import { elephant, bordeaux } from 'src/app/nonogram/levels';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1119',
  templateUrl: './c1119.component.html',
  styleUrls: ['./c1119.component.css']
})
export class C1119Component implements OnInit {
  level: string = bordeaux;
  won: boolean = false;
  lost: boolean = false;

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  win() {
      this.won = true;
      this.giftService.receiveGift();
  }

  nonogramresult(event: boolean) {
    if (!event)
      this.lost = true;
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }

}
