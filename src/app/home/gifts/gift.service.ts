import { Injectable } from '@angular/core';
import { Gift, gifts } from './gifts.model';
import { Router } from '@angular/router';
import { Request } from 'src/app/models/request.model';
import { HttpClient } from '@angular/common/http';
import { challenges, Challenge } from '../challenges/challenges.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GiftService {
  availableGifts = new BehaviorSubject<Gift[]>([]);
  isLoaded = new BehaviorSubject<boolean>(false);
  constructor(private router: Router, private http: HttpClient) {
    this.loadAvailableGifts();
  }

  public isGiftAvailable(giftId: string): boolean {
    return true
    return this.availableGifts.value.findIndex(gift => gift.id === giftId) !== -1;
  }

  public receiveGift() {
    const challenge = challenges.find(c => c.date.getDate() === new Date().getDate() && c.date.getMonth() === new Date().getMonth());
    this.availableGifts.next([...this.availableGifts.value, ...challenge.gifts]);
    this.saveCompletedChallenge(challenge);
    setTimeout(() => {
      this.router.navigateByUrl('gift/' + challenge.gifts[0].url);
    }, 5000);
  }

  private loadAvailableGifts() {
    new Request<string[]>(this.http).get('/teo/challenge').then(completed => {
      let gifts = [];
      challenges.filter(c => completed.includes(c.id)).forEach(c => gifts = [...gifts, ...c.gifts]);
      this.availableGifts.next(gifts);
      this.isLoaded.next(true);
    });
  }

  private saveCompletedChallenge(challenge: Challenge) {
    new Request<string[]>(this.http).post('/teo/challenge', {challengeId: challenge.id});
  }
}
