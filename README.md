# Teó birthday
A web application that displays exactly 21 presents, one per a day. The presents can be unlocked by completing the daily challenge, which varies each day.
## Challenges
1. Snake 3x
2. Nonogram 4x
3. Tests 5x
4. HTML 1x
## Gifts
1. Debter background 1x
2. Coupons (Massage, kaja) 3x
3. Pictures (Fitness) 4x
4. Tickets (Színhaz) 1x
5. Snake 1x
6. Password 1x
7. Drawing 1x
8. Song? 1x
## Kérdések
- Hol olvasta a cikket Péter?
  - Újságban
  - Ja nem is, talán inkább az interneten
  - Lehet, hogy sport volt
  - Nem tudom már, de érdekes volt
- Mennyivel lettél idősebb nálam?
  - 48 nappal
  - 49 nappal
  - 1 évvel
  - Eddig is ugyanannyival voltál idősebb nálam
- Milyen tárgyam nem volt?
  - Szoftvertechnológia
  - Szoftvermodellezés
  - Rendszerelmélet
  - Szovtvertechnikák
- Ki a legnépszerűbb előadó a Data Is Beautiful szerint 2019-ben?
  - Drake
  - Justin Bieber
  - Ed Sheeran
  - Imagine Dragons