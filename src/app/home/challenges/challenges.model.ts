import { Gift, gifts } from '../gifts/gifts.model';

export interface Challenge {
  url: string;
  gifts: Gift[];
  date: Date;
  id: string;
}

export const challenges: Challenge[] = [
  { url: '1113', date: new Date(2019, 10, 13), gifts: [gift('debter'), gift('nonogram-twentyone')], id: 'c1113' },
  { url: '1114', date: new Date(2019, 10, 14), gifts: [gift('snake')], id: 'c1114' },
  { url: '1115', date: new Date(2019, 10, 15), gifts: [gift('password'), gift('snake15')], id: 'c1115' },
  { url: '1116', date: new Date(2019, 10, 16), gifts: [gift('image0'), gift('nonogram-elephant')], id: 'c1116' },
  { url: '1117', date: new Date(2019, 10, 17), gifts: [gift('coupon0')], id: 'c1117' },
  { url: '1118', date: new Date(2019, 10, 18), gifts: [gift('image1')], id: 'c1118' },
  { url: '1119', date: new Date(2019, 10, 19), gifts: [gift('coupon1'), gift('nonogram-bordeaux')], id: 'c1119' },
  { url: '1120', date: new Date(2019, 10, 20), gifts: [gift('szinhaz'), gift('snake-20')], id: 'c1120' },
  { url: '1121', date: new Date(2019, 10, 21), gifts: [gift('image3'), gift('nonogram-lama')], id: 'c1121' },
  { url: '1122', date: new Date(2019, 10, 22), gifts: [gift('coupon2'), gift('nonogram-louvre')], id: 'c1122' },
  { url: '1123', date: new Date(2019, 10, 23), gifts: [gift('multiplier')], id: 'c1123' },
  { url: '1124', date: new Date(2019, 10, 24), gifts: [gift('image2')], id: 'c1124' },
  { url: '1125', date: new Date(2019, 10, 25), gifts: [{url: '', id: '', name: '', type: 'debter'}], id: 'c1125' }
];

export function gift(id: string): Gift {
  return gifts.find(g => g.id === id);
}
