import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import localeHungary from '@angular/common/locales/hu';
import localeUS from '@angular/common/locales/en';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { registerLocaleData} from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './home/index/index.component';
import { HeaderComponent } from './home/header/header.component';
import { TestComponent } from './home/challenge_templates/test/test.component';
import { C1113Component } from './home/challenges/c1113/c1113.component';
import { DailyChallengeComponent } from './home/daily-challenge/daily-challenge.component';
import { E404Component } from './home/e404/e404.component';
import { GiftImage0Component } from './home/gifts/gift-image0/gift-image0.component';
import { GiftReceivedComponent } from './home/gift/gift-received/gift-received.component';
import { FireworksComponent } from './home/challenge_templates/fireworks/fireworks.component';
import { PictureComponent } from './home/gifts/templates/picture/picture.component';
import { MyComponentsComponent } from './home/my-components/my-components.component';
import { CompletedComponent } from './home/challenges/completed/completed.component';
import { SnakeModule } from './snake/snake.module';
import { SnakeChallengeComponent } from './home/challenge_templates/snake-challenge/snake-challenge.component';
import { NonogramModule } from './nonogram/nonogram.module';
import { C1110Component } from './home/challenges/c1110/c1110.component';
import { C1111Component } from './home/challenges/c1111/c1111.component';
import { QuestionComponent } from './home/challenge_templates/question/question.component';
import { DebterComponent } from './home/gifts/debter/debter.component';
import { C1114Component } from './home/challenges/c1114/c1114.component';
import { C1115Component } from './home/challenges/c1115/c1115.component';
import { C1116Component } from './home/challenges/c1116/c1116.component';
import { C1117Component } from './home/challenges/c1117/c1117.component';
import { C1118Component } from './home/challenges/c1118/c1118.component';
import { PasswordComponent } from './home/gifts/password/password.component';
import { AboutComponent } from './home/about/about.component';
import { CookingCouponComponent } from './home/gifts/cooking-coupon/cooking-coupon.component';
import { C1119Component } from './home/challenges/c1119/c1119.component';
import { Images1Component } from './home/gifts/images1/images1.component';
import { Images2Component } from './home/gifts/images2/images2.component';
import { Images3Component } from './home/gifts/images3/images3.component';
import { PattanasCouponComponent } from './home/gifts/pattanas-coupon/pattanas-coupon.component';
import { MassageCouponComponent } from './home/gifts/massage-coupon/massage-coupon.component';
import { SzinhazComponent } from './home/gifts/szinhaz/szinhaz.component';
import { C1120Component } from './home/challenges/c1120/c1120.component';
import { C1121Component } from './home/challenges/c1121/c1121.component';
import { C1122Component } from './home/challenges/c1122/c1122.component';
import { C1123Component } from './home/challenges/c1123/c1123.component';
import { MultiplierCouponComponent } from './home/gifts/multiplier-coupon/multiplier-coupon.component';
import { C1124Component } from './home/challenges/c1124/c1124.component';
import { C1125Component } from './home/challenges/c1125/c1125.component';
registerLocaleData (localeHungary, 'hu');
registerLocaleData (localeUS, 'en');

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IndexComponent,
    HeaderComponent,
    TestComponent,
    C1113Component,
    DailyChallengeComponent,
    E404Component,
    GiftImage0Component,
    GiftReceivedComponent,
    FireworksComponent,
    PictureComponent,
    MyComponentsComponent,
    CompletedComponent,
    SnakeChallengeComponent,
    C1110Component,
    C1111Component,
    QuestionComponent,
    DebterComponent,
    C1114Component,
    C1115Component,
    C1116Component,
    C1117Component,
    C1118Component,
    PasswordComponent,
    AboutComponent,
    CookingCouponComponent,
    C1119Component,
    Images1Component,
    Images2Component,
    Images3Component,
    PattanasCouponComponent,
    MassageCouponComponent,
    SzinhazComponent,
    C1120Component,
    C1121Component,
    C1122Component,
    C1123Component,
    MultiplierCouponComponent,
    C1124Component,
    C1125Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SnakeModule,
    NonogramModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'hu' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
