export type GiftType = 'picture' | 'snake' | 'nonogram' | 'debter' | 'password' | 'coupon';

export interface Gift {
  url: string;
  id: string;
  name: string;
  type: GiftType;
}

export const gifts: Gift[] = [
  { url: 'debter', id: 'debter', name: 'Debter', type: 'debter' },
  { url: 'nonogram-twentyone', id: 'nonogram-twentyone', name: 'Twenty-one', type: 'nonogram' },
  { url: 'nonogram-elephant', id: 'nonogram-elephant', name: 'Elephant', type: 'nonogram' },
  { url: 'nonogram-bordeaux', id: 'nonogram-bordeaux', name: 'Bordeaux', type: 'nonogram' },
  { url: 'nonogram-louvre', id: 'nonogram-louvre', name: 'Louvré', type: 'nonogram' },
  { url: 'nonogram-lama', id: 'nonogram-lama', name: 'Lama', type: 'nonogram' },
  { url: 'snake', id: 'snake', name: 'Snake', type: 'snake'},
  { url: 'snake15', id: 'snake15', name: 'Snake 15', type: 'snake'},
  { url: 'snake-20', id: 'snake-20', name: 'Snake 20', type: 'snake'},
  { url: 'password', id: 'password', name: 'Password', type: 'password' },
  { url: 'images613wqu', id: 'image0', name: 'Italy', type: 'picture' },
  { url: 'imagesw73g5t9g', id: 'image1', name: 'Schönherz', type: 'picture' },
  { url: 'images84ehuhgn4', id: 'image2', name: 'Home', type: 'picture' },
  { url: 'imageswjfr44lje', id: 'image3', name: 'Home 2', type: 'picture' },
  { url: 'coupon7g34jh32jb', id: 'coupon0', name: 'Cooking coupon', type: 'coupon' },
  { url: 'coupon98zh3276b', id: 'coupon1', name: 'Coupon', type: 'coupon' },
  { url: 'coupon7263bj87', id: 'coupon2', name: 'Massage coupon', type: 'coupon' },
  { url: 'coupon7uzbi35i35', id: 'szinhaz', name: 'Theatre', type: 'coupon' },
  { url: 'coupon7u678hki35', id: 'multiplier', name: 'Multiplier', type: 'coupon' }
  // { url: 'image1', id: 'image1', name: 'Picture 1', type: 'picture' },
  // { url: 'nonogram-twentyone', id: 'nonogramTwentyOne', name: 'Twenty-one', type: 'nonogram' },
  // { url: 'snake', id: 'snake', name: 'Snake', type: 'snake' }
];
