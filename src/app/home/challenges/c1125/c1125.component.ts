import { Component, OnInit } from '@angular/core';
import { saska } from 'src/app/nonogram/levels';

@Component({
  selector: 'app-c1125',
  templateUrl: './c1125.component.html',
  styleUrls: ['./c1125.component.css']
})
export class C1125Component implements OnInit {
  level: string = saska;
  won: boolean = false;
  lost: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  win() {
      this.won = true;
  }

  nonogramresult(event: boolean) {
    if (!event)
      this.lost = true;
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }
}
