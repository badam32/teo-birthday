import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  date: string = (new Date().getMonth() + 1) + '' + new Date().getDate();
  constructor() { }

  ngOnInit() {
  }

}
