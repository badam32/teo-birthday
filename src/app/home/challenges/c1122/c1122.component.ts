import { Component, OnInit } from '@angular/core';
import { louvre } from 'src/app/nonogram/levels';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1122',
  templateUrl: './c1122.component.html',
  styleUrls: ['./c1122.component.css']
})
export class C1122Component implements OnInit {

  level: string = louvre;
  won: boolean = false;
  lost: boolean = false;

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  win() {
      this.won = true;
      this.giftService.receiveGift();
  }

  nonogramresult(event: boolean) {
    if (!event)
      this.lost = true;
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }

}
