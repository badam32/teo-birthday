export interface Block {
  cells: Cell[];
  done: boolean;
}
export class Row {
  blocks: Block[] = [];
  done: boolean = false;
  cells: Cell[] = [];

  addCell (cell: Cell) {
    this.cells.push (cell);
  }

  calculateBlocks() {
    let cells: Cell[] = [];
    for (const cell of this.cells) {
      if (cell.value) cells.push(cell);
      else if (cells.length > 0) {
        this.blocks.push({cells, done: false});
        cells = [];
      }
    }
    if (cells.length > 0)
      this.blocks.push({cells, done: false});
  }

  cellFound() {
    if (this.cells.every(c => c.found || !c.value)) {
      this.done = true;
      this.blocks.forEach(b => b.done = true);
    }

    for (let i = 0; i < this.cells.length; ++i) {
      if (!this.cells[i].found) break;
      const block = this.blocks.find(b => this.cells[i] === b.cells[b.cells.length - 1]);
      if (block != null) block.done = true;
    }

    for (let i = this.cells.length - 1; i >= 0; --i) {
      if (!this.cells[i].found) break;
      const block = this.blocks.find(b => this.cells[i] === b.cells[0]);
      if (block != null) block.done = true;
    }
  }
}

class Column extends Row {}

export class Cell {
  row: Row;
  column: Column;
  value: boolean;
  found: boolean;
  table: Table;
  i: number;
  j: number;

  constructor(value: boolean, row: Row, column: Column, table: Table, i: number, j: number) {
    this.value = value;
    this.row = row;
    this.column = column;
    this.i = i;
    this.j = j;
    this.table = table;
    row.addCell(this);
    column.addCell(this);
  }

  selected(value: boolean) {
    if (this.found) return;
    this.found = true;
    this.row.cellFound();
    this.column.cellFound();
    if (this.value !== value)
      this.table.looseLife();
  }
}

export class Table {
  width: number = 15;
  height: number = 15;
  matrix: Cell[][];
  rows: Row[];
  columns: Column[];
  lifes: number = 3;

  constructor(matrixString: string) {
    this.matrix = new Array(this.width);
    for (let i = 0; i < this.matrix.length; ++i) this.matrix[i] = new Array(this.height);
    this.rows = new Array(this.height);
    for (let i = 0; i < this.rows.length; ++i) this.rows[i] = new Row;
    this.columns = new Array(this.width);
    for (let i = 0; i < this.columns.length; ++i) this.columns[i] = new Column;

    const rowStrings: string[] = matrixString.split('\n');
    rowStrings.forEach((row, j) => {
      for (let i = 0; i < row.length; ++i) {
        if (row.charAt(i) === ' ')
          this.matrix[i][j] = new Cell(false, this.rows[j], this.columns[i], this, i, j);
        else
          this.matrix[i][j] = new Cell(true, this.rows[j], this.columns[i], this, i, j);
      }
    });
    for (let i = 0; i < 15; ++i) {
      this.rows[i].calculateBlocks();
      this.columns[i].calculateBlocks();
    }
  }

  looseLife() {
    this.lifes--;
  }
}
