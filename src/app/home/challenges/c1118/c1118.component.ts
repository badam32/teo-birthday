import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1118',
  templateUrl: './c1118.component.html',
  styleUrls: ['./c1118.component.css']
})
export class C1118Component implements OnInit {

  constructor(private giftService: GiftService) { }
  won = false;
  ngOnInit() {
  }


  win() {
    this.won = true;
    this.giftService.receiveGift();
  }

  stringresult(event: boolean) {
    if (event) {
      this.win();
    }
  }

}
