import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { GiftService } from '../gifts/gift.service';
import { challenges } from './challenges.model';

@Injectable({
  providedIn: 'root'
})
export class CompletedChallengeGuardService implements CanActivate {

  constructor(private router: Router, private giftService: GiftService) { }
  canActivate(): Promise<boolean> | boolean {
    // return true;
    const today = new Date();
    const todayChallenge = challenges.find(c => today.getDate() === c.date.getDate() && today.getMonth() === c.date.getMonth());
    return new Promise<boolean>(resolve => {
      if (!this.giftService.isLoaded.value) {
        this.giftService.isLoaded.subscribe(isLoaded => {
          if (!isLoaded) return;
          if (this.giftService.availableGifts.value.findIndex(g => g.id === todayChallenge.gifts[0].id) === -1)
            resolve(this.redirect());
        });
      }
      else if (this.giftService.availableGifts.value.findIndex(g => g.id === todayChallenge.gifts[0].id) === -1)
        resolve(this.redirect());
      resolve(true);
    });
  }

  private redirect(): boolean {
    this.router.navigateByUrl('/challenge/' + (new Date().getMonth() + 1) + '' + new Date().getDate());
    return false;
  }
}
