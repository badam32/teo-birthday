import { Component, OnInit } from '@angular/core';
import { GiftService } from './gifts/gift.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private g: GiftService) { }

  ngOnInit() {
  }

}
