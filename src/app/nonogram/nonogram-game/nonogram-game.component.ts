import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Table, Cell } from '../nonogram.model';
import { ActivatedRoute } from '@angular/router';
import { elephant } from '../levels';

@Component({
  selector: 'app-nonogram-game',
  templateUrl: './nonogram-game.component.html',
  styleUrls: ['./nonogram-game.component.css']
})
export class NonogramGameComponent implements OnInit {
  @Output() result = new EventEmitter<boolean>();
  readonly size = 30;
  table: Table;
  cells: Cell[] = [];
  value: boolean = true;
  lost: boolean = false;
  mouseDown: boolean = false;
  selectedCell: Cell = null;

  @Input() level: string = null;

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.level === null) {
      this.level = this.activatedRoute.snapshot.data.level;
    }
    this.table = new Table(
this.level);
    this.cells = this.table.matrix.reduce((acc, current) => [...acc, ...current], []);
  }

  select(cell: Cell) {
    if (!this.mouseDown) return;
    if (this.lost) return;
    if (cell.found) return;
    cell.selected(this.value);
    this.selectedCell = cell;
    if (this.table.lifes === 0) {
      this.lost = true;
      this.result.next(false);
    }
    else if (this.table.rows.every(r => r.done)) {
      this.result.next(true);
    }
  }

  onMouseDown(cell: Cell) {
    this.mouseDown = true;
    this.select(cell);
  }

  switch() {
    this.selectedCell = null;
    this.value = !this.value;
  }

}
