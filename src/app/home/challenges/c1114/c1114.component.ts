import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1114',
  templateUrl: './c1114.component.html',
  styleUrls: ['./c1114.component.css']
})
export class C1114Component implements OnInit {

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result(b: boolean) {
    if (b) {
      this.giftService.receiveGift();
    }
  }

}
