import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-snake-challenge',
  templateUrl: './snake-challenge.component.html',
  styleUrls: ['./snake-challenge.component.css']
})
export class SnakeChallengeComponent implements OnInit {

  @Input() minScore: number;
  @Input() speed: number;
  @Input() size: number;
  @Output() result = new EventEmitter<boolean>();
  score: number = 0;
  constructor(private link: ActivatedRoute) { }

  ngOnInit() {
    if (!this.speed) {
      this.minScore = this.link.snapshot.data.minScore;
      this.speed = this.link.snapshot.data.speed;
      this.size = this.link.snapshot.data.size;
    }
  }

  grown() {
    this.score++;
    if (this.minScore === this.score) {
      this.result.emit(true);
    }
  }
  lost() {
    this.result.emit(false);
  }
}
