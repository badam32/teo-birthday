import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { challenges } from '../../challenges/challenges.model';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  @Input() question: string;
  @Input() answers: string[];
  @Input() correct: number[];
  won = false;
  @Output() result = new EventEmitter<boolean>();

  selected: number = -1;
  sent: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  select(index: number) {
    if (!this.sent)
      this.selected = index;
  }

  send() {
    if (this.selected === -1 || this.sent) return;
    this.won = this.correct.includes(this.selected);
    if (this.won) this.win();
    this.result.emit(this.won);
    this.sent = true;
  }

  win() {
  }

}
