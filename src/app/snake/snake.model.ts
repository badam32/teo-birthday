export class SnakeBodyPart {
  type: 'head' | 'body';
  position: {x: number, y: number};

  constructor(position: {x: number, y: number}, type: 'head' | 'body' = 'body') {
    this.position = position;
    this.type = type;
  }
}

export class Snake {
  body: SnakeBodyPart[];
  constructor(position: {x: number, y: number}) {
    this.body = [new SnakeBodyPart(position, 'head')];
  }


  move(translate: {x: number, y: number}, game: Game ) {
    const head = this.body[0];
    const nextHeadPosition = { x: head.position.x + translate.x, y: head.position.y + translate.y };

    if (nextHeadPosition.x === game.board.apple.x && nextHeadPosition.y === game.board.apple.y) {
      this.body.push(new SnakeBodyPart({...this.body[this.body.length - 1].position}));
      for (let i = this.body.length - 1; i > 0; --i)
        this.body[i].position = { ...this.body[i - 1].position };
      game.board.generateNewApple();
    }
    else if (
      nextHeadPosition.x < 0 || nextHeadPosition.y < 0 ||
      nextHeadPosition.x >= game.board.width || nextHeadPosition.y >= game.board.height
    )
      game.loose();
    else
      for (let i = this.body.length - 1; i > 0; --i)
        this.body[i].position = { ...this.body[i - 1].position };

    if (this.body.some(part => part !== head && part.position.x === nextHeadPosition.x && part.position.y === nextHeadPosition.y))
      game.loose();

    if (!game.lost)
      head.position = nextHeadPosition;
  }
}

export class Board {
  width: number;
  height: number;
  apple: {x: number, y: number};
  foo: any;

  constructor(w: number, h: number, foo) {
    this.width = w;
    this.height = h;
    this.foo = foo;
  }

  generateNewApple() {
    this.foo();
    this.apple = {
      x: this.getRandomInt(0, this.width - 1),
      y: this.getRandomInt(0, this.height - 1)
    };
  }

  private getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}


export class Game {
  board: Board;
  snake: Snake;
  nextTranslation: {x: number, y: number} = {x: 0, y: 0};
  prevTranslation: {x: number, y: number} = {x: 0, y: 0};
  lost: boolean = false;
  lostGameFun: any;
  interval = 200;

  constructor(size, interval, foo, lost) {
    this.board = new Board(size, size, foo);
    this.snake = new Snake({x: Math.round(size / 2), y: Math.round(size / 2)});
    this.lostGameFun = lost;
    this.interval = interval;
    this.run();
  }

  setNextTranslation(dir: 'LEFT' | 'RIGHT' | 'UP' | 'DOWN') {
    const translations = {
      'LEFT' : { x: -1, y: 0 },
      'RIGHT' : { x: 1, y: 0 },
      'UP' : { x: 0, y: -1 },
      'DOWN' : { x: 0, y: 1 }
    };

    if (translations[dir].x * this.prevTranslation.x !== -1 && translations[dir].y * this.prevTranslation.y !== -1)
      this.nextTranslation = translations[dir];
  }

  loose() {
    this.lost = true;
    this.lostGameFun();
  }

  private run() {
    this.board.generateNewApple();
    const interval = setInterval(() => {
      if (!this.lost) {
        this.snake.move(this.nextTranslation, this);
        this.prevTranslation = {...this.nextTranslation};
      } else clearInterval(interval);
    }, this.interval);
  }
}
