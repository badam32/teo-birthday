export const elephant: string =
`      XXXX     
    XXX  XX    
   XXXXXX XX   
  XXXXXXX  X   
  XXX XXX XX   
  XXXXXXXXXX   
 XXXXXXXXXXXXX 
 XXXXXXXXXXXXXX
 XXX XXXXXXXXXX
 XX  XXXXXXXXXX
 XX  XXXXXXXXXX
 XX  XXXXXXXXXX
XXX  XXX    XXX
XX   XXX    XXX
     XXX    XXX`;

export const twentyOne: string =
`  XXXX     XXXX
 XXXXXXX  XXXXX
XXXXXXXXXXXXXXX
XXXXXXXXXXXXXXX
XXX  XXXXXXXXXX
     XXXXXXXXXX
    XXXXXX XXXX
   XXXXX   XXXX
  XXXXX    XXXX
 XXXXX     XXXX
XXXXXX     XXXX
XXXXX      XXXX
XXXXXXXXX  XXXX
XXXXXXXXX  XXXX
XXXXXXXXX  XXXX`;
export const bordeaux: string =
`              X
XXXXXXXXXX   XX
XXXXXXXXXXX XXX
XXXXXXXXXXXXXXX
          XXXXX
 X X X X XXX   
          XX   
XXXXXXXXXXXXXXX
          XX   
 X X X X XXX   
          XXXXX
XXXXXXXXXXXXXXX
XXXXXXXXXXX XXX
XXXXXXXXXX   XX
              X`;

export const louvre =
`          X    
         XXX   
         X XX  
        XXXXXX 
       XX  X X 
      X  X X  X
     XXXXXXXXXX
     XX   X    
    XX X X XX X
   XX   X    X 
  XXXXXXXXXXXXX
 XXX    X    X 
XX  X XX XX X  
X    X     X   
XXXXXXXXXXXXXXX`;

export const lama = 
`  XXXX       XX
 XX  X    X XX 
 X   XX   X XX 
 X   XX X X XX 
 X   XX XXXXX  
 XX   XXXXX    
  XX           
   XX  X   X  X
   X  XX   XX  
  XX           
  XXXXXXXXXXXXX
  XXXXXXXXXXXXX
  XXXXXXXXXXXXX
  XXXXXXXXXXXXX
   X XXXXXX  XX
`;
export const saska =
`      XXXXXX   
       XXXXX   
       XXXX    
        XX     
   X    XX     
  XX   XXXX    
  XXX  XXXX    
 XXXX XXXXXX   
XX XXXXXXXXX   
X  XXXXXXXXXX  
X   XXXX XXXX  
X   XXXX  XXXXX
    XXX   XXXXX
     X     XXXX
           XXXX
`;