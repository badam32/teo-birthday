import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1123',
  templateUrl: './c1123.component.html',
  styleUrls: ['./c1123.component.css']
})
export class C1123Component implements OnInit {

  won: boolean = false;
  question: number = 0;
  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result1(b: boolean) {
    if (b) {
      setTimeout(() => {
        this.question = 1;
      }, 2000);
    }
  }

  result(b: boolean) {
    if (b && !this.won) {
      this.won = b;
      this.giftService.receiveGift();
    }
  }

}
