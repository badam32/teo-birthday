import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './home/index/index.component';
import { DailyChallengeComponent } from './home/daily-challenge/daily-challenge.component';
import { ChallengeGuardService } from './home/challenges/challenge-guard.service';
import { E404Component } from './home/e404/e404.component';
import { challenges, Challenge } from './home/challenges/challenges.model';
import { GiftReceivedComponent } from './home/gift/gift-received/gift-received.component';
import { gifts, Gift } from './home/gifts/gifts.model';
import { MyComponentsComponent } from './home/my-components/my-components.component';
import { CompletedChallengeGuardService } from './home/challenges/completed-challenge-guard.service';
import { CompletedComponent } from './home/challenges/completed/completed.component';
import { routes } from './app.routes';



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
