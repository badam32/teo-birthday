import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { twentyOne } from 'src/app/nonogram/levels';
import { GiftService } from '../../gifts/gift.service';
@Component({
  selector: 'app-c1113',
  templateUrl: './c1113.component.html',
  styleUrls: ['./c1113.component.css']
})
export class C1113Component implements OnInit {
  level = twentyOne;

  won: boolean = false;
  lost: boolean = false;
  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result(event: boolean) {
    if (event) {
      this.won = true;
      this.giftService.receiveGift();
      // setTimeout(() => {
      //   window.location.href = 'https://debter.xyz';
      // }, 5000);
    }
      else this.lost = true;
  }
}
