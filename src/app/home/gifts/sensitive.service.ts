import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SensitiveService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(): boolean | Promise<boolean> {
    const res = prompt('Mi a jelszó?');
    if (res === 'AdamALegjobb2019') return true;
    this.router.navigateByUrl('/my-collection')
    return false;
  }
}
