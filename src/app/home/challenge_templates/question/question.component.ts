import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() question: string;
  @Input() answers: string[];
  @Input() placeholder: string;

  @Output() result = new EventEmitter<boolean>();

  try: boolean = false;
  constructor() { }

  ngOnInit() {
  }


  send (str: string) {
    if (this.answers.includes(str.toLocaleLowerCase())) {
      this.result.next(true);
      this.try = false;
    }
    else {
      this.result.next(false);
      this.try = true;
    }
  }
}
