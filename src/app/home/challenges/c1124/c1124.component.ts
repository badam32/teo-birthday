import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1123',
  templateUrl: './c1124.component.html',
  styleUrls: ['./c1124.component.css']
})
export class C1124Component implements OnInit {

  won: boolean = false;
  question: number = 0;
  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result1(b: boolean) {
    if (b) {
      setTimeout(() => {
        this.question = 1;
      }, 2000);
    }
  }

  result(b: boolean) {
    if (b && !this.won) {
      this.won = b;
      this.giftService.receiveGift();
    }
  }

}
