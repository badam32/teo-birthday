import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-debter',
  template: '<h2 style="color: white; font-size: 2rem">Redirecting in {{ countDown }}</h2>',
  styleUrls: []
})
export class DebterComponent implements OnInit, OnDestroy {

  countDown = 3;
  interval: any;
  constructor() { }

  ngOnInit() {
    this.interval = setInterval(() => { this.countDown--; if (this.countDown === 0)  window.location.href = 'https://debter.xyz'; }, 800);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }

}
