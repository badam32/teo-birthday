import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gift } from '../gifts/gifts.model';
import { GiftService } from '../gifts/gift.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-my-components',
  templateUrl: './my-components.component.html',
  styleUrls: ['./my-components.component.css']
})
export class MyComponentsComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  gifts: Gift[];
  constructor(private giftService: GiftService) {
  }

  ngOnInit() {
    this.subscription = this.giftService.availableGifts.subscribe(gifts => {
      this.gifts = gifts.filter(g => g.type !== 'password');
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
