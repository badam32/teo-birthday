import { Component, OnInit } from '@angular/core';
import { GiftService } from '../../gifts/gift.service';

@Component({
  selector: 'app-c1117',
  templateUrl: './c1117.component.html',
  styleUrls: ['./c1117.component.css']
})
export class C1117Component implements OnInit {

  constructor(private giftService: GiftService) { }

  ngOnInit() {
  }

  result(b: boolean) {
    if (b) {
      this.giftService.receiveGift();
    }
  }
}
