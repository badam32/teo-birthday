import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './home/index/index.component';
import { DailyChallengeComponent } from './home/daily-challenge/daily-challenge.component';
import { CompletedComponent } from './home/challenges/completed/completed.component';
import { CompletedChallengeGuardService } from './home/challenges/completed-challenge-guard.service';
import { GiftReceivedComponent } from './home/gift/gift-received/gift-received.component';
import { gifts } from './home/gifts/gifts.model';
import { MyComponentsComponent } from './home/my-components/my-components.component';
import { E404Component } from './home/e404/e404.component';
import { NonogramGameComponent } from './nonogram/nonogram-game/nonogram-game.component';
import { C1113Component } from './home/challenges/c1113/c1113.component';
import { ChallengeGuardService } from './home/challenges/challenge-guard.service';
import { twentyOne, elephant, lama, bordeaux, louvre } from './nonogram/levels';
import { SnakeGameComponent } from './snake/snake-game/snake-game.component';
import { DebterComponent } from './home/gifts/debter/debter.component';
import { C1114Component } from './home/challenges/c1114/c1114.component';
import { C1115Component } from './home/challenges/c1115/c1115.component';
import { C1116Component } from './home/challenges/c1116/c1116.component';
import { PasswordComponent } from './home/gifts/password/password.component';
import { AboutComponent } from './home/about/about.component';
import { GiftImage0Component } from './home/gifts/gift-image0/gift-image0.component';
import { SnakeChallengeComponent } from './home/challenge_templates/snake-challenge/snake-challenge.component';
import { CookingCouponComponent } from './home/gifts/cooking-coupon/cooking-coupon.component';
import { C1117Component } from './home/challenges/c1117/c1117.component';
import { C1118Component } from './home/challenges/c1118/c1118.component';
import { C1119Component } from './home/challenges/c1119/c1119.component';
import { Images1Component } from './home/gifts/images1/images1.component';
import { Images2Component } from './home/gifts/images2/images2.component';
import { Images3Component } from './home/gifts/images3/images3.component';
import { SensitiveService } from './home/gifts/sensitive.service';
import { PattanasCouponComponent } from './home/gifts/pattanas-coupon/pattanas-coupon.component';
import { MassageCouponComponent } from './home/gifts/massage-coupon/massage-coupon.component';
import { SzinhazComponent } from './home/gifts/szinhaz/szinhaz.component';
import { C1120Component } from './home/challenges/c1120/c1120.component';
import { C1121Component } from './home/challenges/c1121/c1121.component';
import { C1122Component } from './home/challenges/c1122/c1122.component';
import { C1123Component } from './home/challenges/c1123/c1123.component';
import { MultiplierCouponComponent } from './home/gifts/multiplier-coupon/multiplier-coupon.component';
import { C1125Component } from './home/challenges/c1125/c1125.component';

const giftComponents: {path: string, component: any, data?: {}, canActivate?: any[]}[] = [
  { path: 'debter', component: DebterComponent},
  { path: 'nonogram-twentyone', component: NonogramGameComponent, data: {level: twentyOne}},
  { path: 'nonogram-elephant', component: NonogramGameComponent, data: {level: elephant}},
  { path: 'nonogram-bordeaux', component: NonogramGameComponent, data: {level: bordeaux}},
  { path: 'nonogram-louvre', component: NonogramGameComponent, data: {level: louvre}},
  { path: 'nonogram-lama', component: NonogramGameComponent, data: {level: lama}},
  { path: 'snake', component: SnakeGameComponent },
  { path: 'snake15', component: SnakeChallengeComponent, data: {minScore: 15, speed: 100, size: 15} },
  { path: 'snake-20', component: SnakeChallengeComponent, data: {minScore: 20, speed: 200, size: 25} },
  { path: 'password', component: PasswordComponent },
  { path: 'images613wqu', component: GiftImage0Component, canActivate: [SensitiveService]},
  { path: 'imagesw73g5t9g', component: Images1Component, canActivate: [SensitiveService] },
  { path: 'images84ehuhgn4', component: Images2Component, canActivate: [SensitiveService] },
  { path: 'imageswjfr44lje', component: Images3Component, canActivate: [SensitiveService] },
  { path: 'coupon98zh3276b', component: PattanasCouponComponent, canActivate: [SensitiveService] },
  { path: 'coupon7263bj87', component: MassageCouponComponent },
  { path: 'coupon7g34jh32jb', component: CookingCouponComponent },
  { path: 'coupon7uzbi35i35', component: SzinhazComponent },
  { path: 'coupon7u678hki35', component: MultiplierCouponComponent }
];

export const routes: Routes = [
  { path: '', pathMatch: 'prefix', component: HomeComponent, children: [
    { path: '', pathMatch: 'full', component: IndexComponent },
    { path: 'challenge', pathMatch: 'prefix', component: DailyChallengeComponent, children: [
      { path: '1113', component: C1113Component, data: { day: new Date(2019, 10, 13) }, canActivate: [ChallengeGuardService] },
      { path: '1114', component: C1114Component, data: { day: new Date(2019, 10, 14) }, canActivate: [ChallengeGuardService] },
      { path: '1115', component: C1115Component, data: { day: new Date(2019, 10, 15) }, canActivate: [ChallengeGuardService] },
      { path: '1116', component: C1116Component, data: { day: new Date(2019, 10, 16) }, canActivate: [ChallengeGuardService] },
      { path: '1117', component: C1117Component, data: { day: new Date(2019, 10, 17) }, canActivate: [ChallengeGuardService] },
      { path: '1118', component: C1118Component, data: { day: new Date(2019, 10, 18) }, canActivate: [ChallengeGuardService] },
      { path: '1119', component: C1119Component, data: { day: new Date(2019, 10, 19) }, canActivate: [ChallengeGuardService] },
      { path: '1120', component: C1120Component, data: { day: new Date(2019, 10, 20) }, canActivate: [ChallengeGuardService] },
      { path: '1121', component: C1121Component, data: { day: new Date(2019, 10, 21) }, canActivate: [ChallengeGuardService] },
      { path: '1122', component: C1122Component, data: { day: new Date(2019, 10, 22) }, canActivate: [ChallengeGuardService] },
      { path: '1123', component: C1123Component, data: { day: new Date(2019, 10, 23) }, canActivate: [ChallengeGuardService] },
      { path: '1125', component: C1125Component, data: { day: new Date(2019, 10, 25) }, canActivate: [ChallengeGuardService] }
    ]},
    { path: 'completed', pathMatch: 'full', component: CompletedComponent, canActivate: [CompletedChallengeGuardService] },
    { path: 'gift', pathMatch: 'prefix', component: GiftReceivedComponent, children:
      gifts.map(g => ( { ...(giftComponents.find(gift => gift.path === g.url)) }))
    },
    { path: 'my-collection', component: MyComponentsComponent },
    { path: 'about', component: AboutComponent },
    { path: '404', component: E404Component },
    { path: '**', redirectTo: '/404' }
  ]}
];
